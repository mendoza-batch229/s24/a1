// Exponent Operator
let  getCube = 2**3; 

// Template Literals
console.log(`The cube of 5 is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [ad1,ad2,ad3,ad4] = address;
console.log(`I live in ${ad1} ${ad2} ${ad3} ${ad4}`);


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;
	console.log(`${name} was a ${species}. he weight at ${weight} with a measurement of ${measurement}`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach(arrowFunction => console.log(arrowFunction));


// Javascript Classes

	function Dog(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
	let dog1 = new Dog('Covid',3,'Belgian Shepherd');
	console.log(dog1);
	let dog2 = new Dog('Popol',2,'Belgian Shepherd');
	console.log(dog2);

